#!/usr/bin/python
#
###############################################################################
#                                                                             #
# pyPDNS - A PDNS administration tool, written in pure python.                #
# Copyright (C) 2012 IPFire development team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import backend
import argparse

from i18n import _
from backend import *

# Create main class for the CLI.
class Cli(object):
	def __init__(self):
		self.parser = argparse.ArgumentParser(
			description = _("CLI tool to adminstrate authoritative \
				PowerDNS servers."))

		# Add entry for version displaying.
		self.parser.add_argument('--version', action='version', 
			version='pyPDNS 0.1.1')

		# Add sub-commands.
		self.sub_commands = self.parser.add_subparsers()

		self.parse_command_show()
		#self.parse_command_add()
		#self.parse_command_remove()

		# Finally parse all arguments from the command line and save
		# them.
		self.args = self.parser.parse_args()

		# Map return values from action to functions to proceed.
		self.action2func = {
			"show_all"     : self.handle_show_all,
			"show_domains" : self.handle_show_domains,
			"show_records" : self.handle_show_records,
		}

	def parse_command_show(self):
		# Initialize the "show" command.
		sub_show = self.sub_commands.add_parser("show",
                        help=_("Show domains and records."))

		# Initialize subcommands for show.
		sub_show_subcommands = sub_show.add_subparsers()

		# Impement "show all".
		sub_show_records = sub_show_subcommands.add_parser("all",
			help="Show all domains and records.")
		sub_show_records.add_argument("action", action="store_const", 
			const="show_all")

		# Implement "show records" and pick up given domain.
		sub_show_records = sub_show_subcommands.add_parser("records",
			help="Show records from a given domain.")
		sub_show_records.add_argument("domain", nargs="?",
			help="Show records from a given domain.")
		sub_show_records.add_argument("action", action="store_const",
			const="show_records")

		# Implement "show domains".
		sub_show_domains = sub_show_subcommands.add_parser("domains",
			help="Show all configured domains.")
		sub_show_domains.add_argument("action", action="store_const",
			const="show_domains")

	def run(self):
		action = self.args.action

		try:
			func = self.action2func[action]

		except KeyError:
			raise Exception, "Unhandled action: %s" % action

		else:
			# Initialize DNS module.
			self.dns = DNS(DB)

		return func()

	def handle_show_domains(self):
		"""
		Get all domains and print them.
		"""
		domains = self.dns.get_domains()

		# Print the domains, if there are any configured.
		if not domains:
			print "No domain configured, yet."

		else:
			print "Currently configured domains:"
			for domain in domains:
				print " - %s" % domain.name
		

	def handle_show_records(self):
			# Print message if domain doens't exist.
			if not self.args.domain:
				print " No domain name specified to show \
					records."
				print " Use 'pdns show domains' to get a list \
					from all available domains."
	
				return

			# Print message if domain doens't exist.
			domain = self.dns.get_domain(self.args.domain)

			if not domain:
				print " No such domain, use 'pdns show \
					domains' to get a list of all \
					available domains."

				return

			# Print message if no records have been configured yet.
			if not domain.has_records():
				print " Domain has no records yet."

			else:
				print " Showing records \
					for: %s \n" % self.args.domain

				soa = domain.SOA

				# Check if the domain has a SOA record.
				if not soa:
					print " No Start of Authority \
						(SOA record) created yet. \
						Your domain will not work \
						correctly. \n"

				else:
					# Define table layout for SOA table..
					FORMAT = " %-8s: %s"
					print FORMAT % ("MName", soa.mname)
					print FORMAT % ("E-Mail",soa.email)
					print FORMAT % ("Serial", soa.serial)
					print FORMAT % ("Refresh", soa.refresh)
					print FORMAT % ("Retry", soa.retry)
					print FORMAT % ("Expire", soa.expire)
					print FORMAT % ("Minimum", soa.minimum)

					print "" #Just an emtpy line.
 
					# Define layout for records table.
					FORMAT = " %-24s %-8s %-34s %-10s"

					# Define tables headline for records
					# and print it.
					title = FORMAT % (_("Name"), _("Type"), _("Content"), _("TTL"))
					print title
					print "=" * len(title) # spacing line

					# Display all remaining records,
					# except SOA records.
					for record in domain.records:
						if not record.type == "SOA":
							print FORMAT % (record.dnsname, record.type, record.content, record.ttl)


	def handle_show_all(self):
		# Get all domains and print them
                domains = self.dns.get_domains()

		# Print message if no domain has been configured.
		if not domains:
			print "No domain configured, yet."

			return

		for domain in domains:
			print domain.name

			# Spacing line.
			print "=" * 80

			# Print a message if domain has no records.
			if not domain.has_records():
				print "Domain has no records yet."

			# Print records.
			for record in domain.records:
				if record.type == "SOA":
					FORMAT = ("%-30s %s")
					print FORMAT % (record.dnsname, record.type)
				else:
					FORMAT = ("%-30s %-6s %s")
					print FORMAT % (record.dnsname, record.type, record.content)

			# Just an emty line.
			print ""



cli = Cli()
cli.run()
