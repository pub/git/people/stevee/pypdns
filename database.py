#!/usr/bin/python

import itertools
import logging
import os
import sqlite3

class Row(dict):
	"""A dict that allows for object-like property access syntax."""
	def __getattr__(self, name):
		try:
			return self[name]
		except KeyError:
			raise AttributeError(name)


class Database(object):
	def __init__(self, filename):
		self.filename = filename

		self._db = None	
		self.reconnect()

	def __del__(self):
		self.close()

	def close(self):
		"""Closes this database connection."""
		if self._db is not None:
			self._db.commit()
			self._db.close()
			self._db = None

	def reconnect(self):
		"""Closes the existing database connection and re-opens it."""
		self.close()
		self._db = sqlite3.connect(self.filename)
		self._db.isolation_level = None # Enabled autocommit mode

	def query(self, query, *parameters):
		"""Returns a row list for the given query and parameters."""
		cursor = self._cursor()
		try:
			self._execute(cursor, query, parameters)
			column_names = [d[0] for d in cursor.description]
			return [Row(itertools.izip(column_names, row)) for row in cursor]
		finally:
			#cursor.close()
			pass

	def get(self, query, *parameters):
		"""Returns the first row returned for the given query."""
		rows = self.query(query, *parameters)
		if not rows:
			return None
		elif len(rows) > 1:
			raise Exception("Multiple rows returned for Database.get() query")
		else:
			return rows[0]

	def execute(self, query, *parameters):
		"""Executes the given query, returning the lastrowid from the query."""
		cursor = self._cursor()
		try:
			self._execute(cursor, query, parameters)
			return cursor.lastrowid
		finally:
			pass
			#self._db.commit()
			#cursor.close()

	def _cursor(self):
		if self._db is None:	
			self.reconnect()
		return self._db.cursor()

	def _execute(self, cursor, query, parameters):
		try:
			return cursor.execute(query, parameters)
		except sqlite3.OperationalError:
			logging.error("Error connecting to database on %s", self.filename)
			self.close()
			raise
